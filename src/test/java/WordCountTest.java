import org.junit.Assert;
import org.junit.jupiter.api.function.Executable;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class WordCountTest {

    private WordCount wc;

    @org.junit.jupiter.api.Test
    void mainTest() {
        String[] args = {"text.txt", "a", "taco"};
        try {
            wc.main(args);
        }catch(Exception e){
            Assert.assertTrue(false);
            }
        Assert.assertTrue(true);
    }

    @org.junit.jupiter.api.Test
    void allTogetherTest() throws Exception {
        String[] args = {"text.txt", "a", "taco"};

        Assert.assertTrue(wc.allTogether(args));
    }

    @org.junit.jupiter.api.Test
    void wordStats() throws Exception{

        Scanner input = new Scanner(new File("text.txt"));
        Map<String, Integer> map = new HashMap<>();
        Map<String, Integer> expectedMap = new HashMap<>();
        expectedMap.put("next",2);
        expectedMap.put("a",3);
        expectedMap.put("test",3);
        expectedMap.put("line",2);
        expectedMap.put("this",1);
        expectedMap.put("count",1);
        expectedMap.put("words",1);
        expectedMap.put("is",3);
        expectedMap.put("rd",1);
        expectedMap.put("unique",1);
        expectedMap.put("abcdefghijklmnopqrstuvwxyz",1);
        expectedMap.put("to",1);
        expectedMap.put("dont",2);

        map = wc.wordStats(input);

        Assert.assertEquals(expectedMap, map);

    }

    @org.junit.jupiter.api.Test
    void lineCount() throws Exception {

        Assert.assertEquals(4, wc.lineCount("text.txt"));
    }

    @org.junit.jupiter.api.Test
    void charCount() throws Exception {

        Assert.assertEquals(118, wc.charCount("text.txt"));
    }

    @org.junit.jupiter.api.Test
    void wordReplace()  throws Exception {

        StringBuilder newStrings = new StringBuilder();
        newStrings.append("This is a test to count unique words. is a test IS taco teST. dont don't.\n");
        newStrings.append("Next line nExT\n");
        newStrings.append("3rd line\n");
        newStrings.append("abcdefghijklmnopqrstuvwxyz\n");

        Assert.assertEquals(newStrings.toString(), wc.wordReplace("text.txt", "A", "taco").toString());
    }
}