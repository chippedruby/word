import java.io.File;
import java.util.*;

public class WordCount {

    public static void main(String[] args) {

            allTogether(args);

    }

    public static boolean allTogether(String[] args) {

        Map<String, Integer> map = new HashMap<>();

        try {
            if (args.length < 3) {
                throw new Exception("Please input file path, old string, and new string");
            }

            System.out.println("Project 1: Word Statistics");
            Scanner input = new Scanner(new File(args[0]));

            map = wordStats(input);

            System.out.println(map.toString());
            input.close();
            System.out.println("Lines: " + lineCount(args[0]));
            System.out.println("Chars: " + charCount(args[0]));
            System.out.println("New Text: " + wordReplace(args[0], args[1], args[2]).toString());
        }catch(Exception e){
            System.out.println("Exception: " + e.getMessage());
            return false;
        }
        return true;
    }


    public static Map<String, Integer> wordStats(Scanner input){
        Map<String, Integer> map = new HashMap<>();

        while(input.hasNext()) {
            String word = input.next().toLowerCase().replaceAll("[^a-zA-Z ]", "");

            Integer count = map.get(word);
            count = (count == null) ? 1 : ++count;
            map.put(word, count);

        }

        return map;
    }

    public static int lineCount(String args) throws Exception{

        Scanner input = new Scanner(new File(args));

        int lines = 0;
        while (input.hasNextLine()){

            lines++;
            input.nextLine();
        }

        input.close();
        return lines;
    }

    public static int charCount(String args) throws Exception{

        Scanner input = new Scanner(new File(args));

        int chars = 0;
        while (input.hasNextLine()) {

            chars += input.nextLine().length();

        }

        input.close();
        return chars;
    }

    public static StringBuilder wordReplace(String args, String oldString, String newString) throws Exception{


        StringBuilder newData = new StringBuilder();
        Scanner input = new Scanner(new File(args));
        String newLine = "";
        while (input.hasNextLine()){
            newLine = input.nextLine();
            newData.append(newLine.replaceAll("\\b"+oldString+"\\b", newString));
            newData.append("\n");
        }

        input.close();
        return newData;
    }



}